import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.cam"
version = "1.0-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.3.41"
    application
}

application {
    mainClassName = "MainKt"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(group = "com.sun.mail", name = "javax.mail", version = "1.4.7")
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
    withType<JavaExec> {
        System.getProperties().entries
            .forEach { systemProperty(it.key.toString(), it.value) }
    }
}
