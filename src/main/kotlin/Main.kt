import javax.mail.internet.InternetAddress

fun main() {
    with(Arguments) {
        val client = GmailSMTPClient(username, password)
        client.sendMessage(subject, body, recipient)
    }
}

object Arguments {
    val username: InternetAddress = InternetAddress(System.getProperty("username"))
    val password: String = System.getProperty("password")
    val subject: String = System.getProperty("subject") ?: ""
    val body: String = System.getProperty("body") ?: ""
    val recipient: InternetAddress = InternetAddress(System.getProperty("recipient"))
}
