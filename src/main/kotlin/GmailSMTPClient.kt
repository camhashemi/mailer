import java.util.Properties
import javax.mail.Authenticator
import javax.mail.Message
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

class GmailSMTPClient(val username: InternetAddress, val password: String) {

    fun sendMessage(subject: String, text: String, recipient: InternetAddress) {
        val message =
            MimeMessage(session).apply {
                setSubject(subject)
                setText(text, Charsets.US_ASCII.toString())
                setRecipients(Message.RecipientType.TO, arrayOf(recipient))
            }
        Transport.send(message)
    }

    private val session: Session by lazy {
        val properties = Properties().apply {
            set("mail.smtp.host", "smtp.gmail.com")
            set("mail.smtp.port", "465")
            set("mail.smtp.auth", "true")
            set("mail.smtp.starttls.enable", "true")
            set("mail.smtp.socketFactory.port", "465")
            set("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory")
        }
        val authenticator = object : Authenticator() {
            override fun getPasswordAuthentication(): PasswordAuthentication =
                PasswordAuthentication(username.address, password)
        }

        Session.getInstance(properties, authenticator)
    }
}
